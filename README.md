**417 Dominion – an asynchronous Necromunda Dominion campain**

This campaign is a modification of the Dominion Campaign of the general Necromunda Rulebook of 2018, p78. The main difference is, that territories are not chosen by participants but allocated by chance (overall one time per campaign phase). This way participatns can meet asynchronously (not all together on a certain day but just the matched players for their specific matches on different days and in smaller, more flexible groups).

# General information

## Abbreviations

| Abbreviation       | Meaning |
| ---                | ---     |
| BoJ                | Supplement "Book of Justice" |
| HoS                | Supplement "House of Shadows" |
| OoA                | Out of Action |
| p<123>             | Page number in general Necromunda Rulebook of 2018 |
| Pre-BS             | Pre-Battle Sequence |
| Post-BS            | Post-Battle Sequence |

## Campaign schedule

- Campaign duration: 6 months
    - start: Q4 2022
    - end: Q1 2023.
- Duration of a campaign phase: 3 weeks

| Phase | First phase day | Last phase day  | Note |
| ---   | ---             | ---             | ---  |
| 1     | 2022-09-26 (Mo) | 2023-11-10 (Su) | |
| 2     | 2022-11-11 (Fr) | 2023-12-31 (Sa) | |
| 3     | 2023-01-01 (Su) | 2023-07-01 (Sa) | |

## Participating Gangs

Ordered by registration day:

| Player ID | Gang name                                               | Gang ID | Faction           | Player name (Matrix) | Affiliation                 | Participated in phases | Notes |
| ---       | ---                                                     | ---     | ---               | ---                  | ---                         | ---                    | ---   |
| 1         | Ambers Bots                                             | AmBot   | Outcasts          | lunokhod             | neither (clanless)          | 1                      | Abirtrator |
| 2         | (†) Devoted of the Star Brethren                        | DSB (†) | Genestealer Cults | nyamtsoi             | Outlaw                      | 1                      | Death of Leader in first match, end of DSB (†), new gang: DSC |
| 2         | The Devoted of the Star Children                        | DSC     | Genestealer Cults | nyamtsoi             | Outlaw                      | 1-3                    | -     |
| 3         | His Lord Inquisitor Morgwan's requisitioned precinct 14 | P14     | Enforcers         | the_void             | Law-Abiding                 | 1-3                    | -     | 
| 4         | (†) MILK Raiders                                        | MILK    | House Goliath     | mrrobat              | Law-Abiding                 | 1                      | Left in phase 1 |
| 5         | No Prayer for the Dying                                 | NPFTD   | House Escher      | ducyboy              | Law-Abiding                 | 1-3                    | Left in phase 3 |
| 6         | purity fighters                                         | pf      | House Cawdor      | fry2nice             | Outlaw                      | 1-3                    | -     |
| 7         | ﻿Nachthexen                                              | nh      | House Escher      | bountainmike0815     | Law-Abiding                 | 1                      | left in phase 2?     | 
| 8         | Pussycat Prolls                                         | PP      | House Escher      | ganbariya            | Law-Abiding                 | 2-3                    | -     |
| 9         | Gysman Khans Gang                                       | GKG     | House Delaque     | fluffygismo          | Law-Abiding                 | 3                      | -     |

Because some factions didn't exist when the Dominion campaign was developed (in 2018), some of them count as the following houses for Territory Boons:
* Enforcers: House Goliath
* Genestealer Cults: House Orlock
* Clanless Outcasts: House Orlock

## Important links

- [417 Dominion Campaign Reference](https://gitlab.com/r4dh4l/417dominion/-/blob/main/campaign_reference.pdf)
- Printing templates recommended by the Campaign Arbitrator:
    - [runCFC Fighter Cards](https://gitlab.com/r4dh4l/runcfc)
    - [runCGR Gang Roster](https://gitlab.com/r4dh4l/runcgr)
        - [runCGR Cashflow Sheet](https://gitlab.com/r4dh4l/runcgr/-/blob/main/runcgr_cashflow.pdf)

## Campaign rules

- **Campaign mode**:
    - Dominion campaign
    - Asynchronous mode (no general group dates, participants meet self organized, Territory Fights are matched by chance not by choice)
- **Starting Gang size**: `500 credits`
- **Allowed Factions**: all official GW factions
- **Communication** platform: [[matrix](https://element.io/)]
- **Campaign duration**:
    - from begin of `Q4 2022` until end of `Q1 2023` (6 months)
    - one campaign phase lasts 3 weeks
        - **Matches to play** per phase:
            - one Territory Fight per phase
            - as many Non-Territory Fights as a participant like (with certain limits provided by the specific scenario)
    - 1 week "cool down" between each phase 
- Strict **WYSIWYG** is not forced, but a close combat Fighter with a huge weapon should not be represented by a close combat Fighter with a light weapon, for example.
    - **Conversions** are allowed (playing Sisters of Battle modified for Necromunda in style as a whole House Cawdor gang, for example), proxies not (if you don't have a model to represent a convincing Rogue Doc model you can not use a model of a Space Marine to represent it). Criteria for this is a convincing effect of "immersion".
- Different game special rules:
    - **REP*utation can become negative

For **FAQ and Errata** see section "FAQ and Errata" at the end of this document.

## What to prepare

### When joining the campaign

1. Get the following documents:
    1. Fighter Cards for your gang fighters. The Arbitrator recommends to use [ruNCFC](https://gitlab.com/r4dh4l/runcfc) instead of the official GW Fighter Cards.
    2. A Gang Roster for your gang. The Arbitrator recommends to use [ruNCGR](https://gitlab.com/r4dh4l/runcgr) instead of the official GW Gang Roster.
    3. The [417 Dominion Campaign Reference](https://gitlab.com/r4dh4l/417dominion/-/blob/main/campaign_reference.pdf) for you and just for the case a second time for your opponent. Although under permanent development the Arbitrator recommends to print it before playing a match (it will be a "showstopper" if you read it online and run out of energy during a match).
    4. From [Warhammer Community Download section](https://www.warhammer-community.com/downloads/#necromunda):
        1. **"The Trading Post"** (contains almost all items and all weapon traits)
        2. **"Tokens"** (just if you don't already have Tokens from a starter box, of course)
        3. **"Necromunda Web Exclusive Tactics Cards"** if you don't have own Tactics Cards or, if you like, in addition to your existing Tactics Cards.
     5. The Necromunda Rulebook of 2018 (the Campaign Rules and Reference will refer to chapters in this book sometimes).
     6. From the "Book of Judgement" (2019) the chapter "The Black Market Trading Post" (p93-127).
2. Create a Gang according to the "Campaign gaming rules" section above.
    * Optional: To prevent controversies about the exact front of a model, paint a small dot on the front of the base indicating the middle of the fighters field of vision.
3. Prepare the following documents:
    1. A [fighter card](https://gitlab.com/r4dh4l/runcfc) for any of your gang fighters.
    2. The [Gang Roster](https://gitlab.com/r4dh4l/runcgr).
    3. Optional: Consider to use the [Gang Cashflow sheet](https://gitlab.com/r4dh4l/runcgr/-/blob/main/runcgr_cashflow.pdf) to protocol your credit flow (this will be some more effort in the beginning but will save you a lot of time during the campaign later calcualting the exact Gang Wealth value).
5. Read the [417 Dominion Campaign Reference](https://gitlab.com/r4dh4l/417dominion/-/blob/main/campaign_reference.pdf) to get a first impression how the Pre- and Post-Battle Sequence of this campaign work.

### For any match you play

Follow the steps of the [417 Dominion Campaign Reference](https://gitlab.com/r4dh4l/417dominion/-/blob/main/campaign_reference.pdf). A lot of them you can do **remotely in advance** (schedule match, maybe changing the scenario etc.). Keep in mind that the actual battle is just step 2 ("Battle Sequence") of overall 3 major steps, while the third step is the very important "Post-Battle Sequence" which needs at least as much time as a Battle Sequence.

# Campaign progress

## Territory overview

Maximum territories to occupy: All territories from the official list (p79) can be occupied if not already occupied by another gang.

| Territory                    | Occupied by | Territory Boon | Enhanced Boon |
| ---                          | ---         | ---            | ---           |
| Corpse Farm (Cawdor, p101))  | -           | Income: 1d6x10 credits per fighter removed from Gang Rosters during preceding post-battle sequence | Cawdor: Reputation: +1, Income: Territory Boon but with 2d6 |
| Drinking Hole (Delaque, p102)| GKG         | Reputation: +1 REP; Special: any Ganger may re-roll any failed CL check; if used this  Boon the fighter gets -1 hi penalty for the rest of the battle | Delaque: +2 REP; Special (replaces Territory Boon): The Delaque player may nominate 3 opponent
fighters at start of the battle. These fighters suffer -1 to all tests and checks for the duration of the battle. |
| Fighting Pit (Goliath, p104) | pf          | 2 Hive Scum Hired Gungs for free incl. equipment prior to every battle | Goliath: Reputation: +2 |
| Narco Den (Escher, p104)     | GKG         | Income: 1d6x5 credits | Escher: Reputation: +2, Income: 1d6x5 credits (1d6x10 if owning a Synth Still) |
| Old Ruins (p99)              | DSC         | Income: 1d3x10 credits, +1 to each d3 result for each Dome Runner in your Gang | - |

| Promethium Cache (p99)       | NPFTD       | Equipment: free incendiary charges for 3 fighters, Special: free Anno check re-rolls for Blaze weapons for all fighters | - |
| Toll Crossing (Orlock, p106) | DSC         | Income: 1d6x5 credits | Orlock: Repuration: +1, Special: Territory Boon but with 6 fighters |
| Tunnels (Orlock, p105)       | DSC         | Special: Tunnel deployment for 3 fighters | Orlock: priority in first round of any battle, boon can be bought by any gang for 20 credits for a single battle against another gang |
| Rogue Doc Shop (p99)         | NPFTD       | Recruit: one Rogue Doc Hanger-on for free | - |
| Workshop (p100)              | pf          | Recruit: one Ammo-jack Hanger-on for free | - |

Standard Territory Boons are replaced by Enhanced Boons of the same Boon type if available.

### Hide-outs

Hide-outs are the "homebases" of a gang which can be attacked but never get lost (if attacked successfully the attacking gang gets +5 REP).

| Gang   | Territory                   | Boon                     | Enhanced Boon |
| ---    | ---                         | ---                      | ---           |
| AmBot  | Settlement (p99) | Incomine: 1d6x10 credits; REP: +1; Recruit: 2d6 after every battle, on 6: +1 House List Juve for free, on 12: +1 House List Ganger for free | - |
| DSC  | Settlement (p99) | Incomine: 1d6x10 credits; REP: +1; Recruit: 2d6 after every battle, on 6: +1 House List Juve for free, on 12: +1 House List Ganger for free | - |
| GKG | Settlement (p99) | Incomine: 1d6x10 credits; REP: +1; Recruit: 2d6 after every battle, on 6: +1 House List Juve for free, on 12: +1 House List Ganger for free | - |
| P14  | Palanite Precinct (BoJ p30) | Incomine: 1d6x10 credits | Homeground/Gang Assault: T. can never be occupied by another gang but if successfully assaulted the attacking gang gets +5 REP |
| nh  | Settlement (p99) | Incomine: 1d6x10 credits; REP: +1; Recruit: 2d6 after every battle, on 6: +1 House List Juve for free, on 12: +1 House List Ganger for free | - |
| NPFTD  | Settlement (p99) | Incomine: 1d6x10 credits; REP: +1; Recruit: 2d6 after every battle, on 6: +1 House List Juve for free, on 12: +1 House List Ganger for free | - |
| pf  | Settlement (p99) | Incomine: 1d6x10 credits; REP: +1; Recruit: 2d6 after every battle, on 6: +1 House List Juve for free, on 12: +1 House List Ganger for free | - |
| PP  | Settlement (p99) | Incomine: 1d6x10 credits; REP: +1; Recruit: 2d6 after every battle, on 6: +1 House List Juve for free, on 12: +1 House List Ganger for free | - |

## Ranking

Multiple gangs with the same rank are sorted alphabetically.

**Dominator** (Territories occupied – updated after every match):

| Rank | Gang ID | Overall | P1  | P2  | P3  |
| ---  | ---     | ---     | --- | --- | --- |
| 1    | DSC     | 3       | 1   | 1+1 | 0   |
| 2    | GKG     | 2       | -   | 0+1 | 1   |
| 2    | NPFTD   | 2       | 0   | 1+1 | -   |
| 2    | pf      | 2       | 1   | 0+1 | 0   |
| 3    | nh      | 1       | 0   | 0+1 | 0   |
| 3    | P14     | 1       | 0   | 0+1 | ?   |
| 3    | PP      | 1       | -   | 0+1 | 0   |
| 4    | AmBot   | 0       | 0   | 0   | 0   |
| 4    | DSB (†) | 0       | 0   | -   | -   |
| 4    | MILK    | 0       | 0   | -   | -   |


**Warmonger** (Matches fought (Non-Arena/Hive Bowl) – updated after every match):

| Rank | Gang ID | Overall | P1  | P2  | P3  |
| ---  | ---     | ---     | --- | --- | --- |
| 1    | P14     | 10      | 2   | 5   | 3   |
| 2    | pf      | 7       | 4   | 2   | 1   |
| 3    | DSC     | 6       | 1   | 3   | 2   |
| 4    | GKG     | 4       | -   | 0   | 4   |
| 5    | AmBot   | 2       | 1   | 0   | 1   |
| 5    | PP      | 2       | -   | 2   | 0   |
| 5    | NPFTD   | 2       | 1   | 1   | 0   |
| 6    | DSB (†) | 1       | 1   | 0   | 0   |
| 7    | MILK    | 0       | 0   | 0   | 0   |
| 7    | nh      | 0       | 0   | 0   | 0   |

**Arenamonger** (Arena matches fought – updated after every match):

| Rank | Gang ID | Overall | P1  | P2  |
| ---  | ---     | ---     | --- | --- |
| 1    | P14     | 4       | 1   | 3   |
| 1    | pf      | 4       | 1   | 3   |
| 2    | DSC     | 1       | 0   | 1   |
| 2    | NPFTD   | 1       | 0   | 1   |
| 3    | AmBot   | 0       | 0   | 0   |
| 3    | DSB (†) | 0       | 0   | 0   |
| 3    | GKG     | 0       | -   | -   |
| 3    | MILK    | 0       | 0   | 0   |
| 3    | nh      | 0       | 0   | 0   |
| 3    | PP      | 0       | -   | 0   |

**Powerbroker** (Reputation – updated only at the end of a campaign phase):

| Rank | Gang ID | P1  | P2  |
| ---  | ---     | --- | --- |
| 1    | P14     | 9   | 53  |
| 2    | pf      | 29  | 42  |
| 3    | DSC     | 8   | 32  |
| 4    | NPFTD   | 6   | 14  |
| 5    | DSB (†) | 3   | ?   |
| 6    | AmBot   | 2   | 2   |
| 7    | GKG     | -   | 1   |
| 7    | MILK    | 1   | ?   |
| 7    | nh      | 1   | ?   |
| 7    | PP      | -   | ?   |

**Creditor** (Wealth – updated only at the end of a campaign phase):

| Rank | Gang ID | P1  | P2   |
| ---  | ---     | --- | ---  |
| 1    | pf      | 935 | 1745 |
| 2    | DSC     | 624 | 1696 |
| 3    | P14     | 572 | 1615 |
| 4    | NPFTD   | 548 | 980  |
| 5    | AmBot   | 551 | 641  |
| 6    | MILK    | 500 | -    |
| 6    | nh      | 500 | ???  |
| 6    | GKG     | -   | 500  |
| 6    | PP      | -   | 500  |
| 7    | DSB (†) | 405 | -    |

**Slaughterer** (Enemy fighters taken (directly) Out of Action in Non-Arena matches – updated only at the end of a campaign phase):

| Rank | Gang ID | Overall | P1  | P2  |
| ---  | ---     | ---     | --- | --- |
| 1    | pf      | 17      | 10  | 7   |
| 2    | DSC     | 11      | 1   | 10  |
| 3    | NPFTD   | 10      | 3   | 7   |
| 4    | DSB (†) | 3       | 3   | -   |
| 4    | P14     | 3       | 1   | 2   |
| 5    | AmBot   | 2       | 2   | 0   |
| 6    | MILK    | 0       | 0   | -   |
| 6    | nh      | 0       | 0   | 0   |
| 6    | PP      | 0       | -   | ?   |
| 6    | GKG     | 0       | -   | -   |

**Arena Slaughterer (Gang)** (Enemy fighters taken (directly) Out of Action in Arena matches per **Gang** – updated after every match):

| Rank | Gang ID | Overall | P1  | P2  | P3  |
| ---  | ---     | ---     | --- | --- | --- |
| 1    | P14     | 3       | 1   | 2   | 0   |
| 2    | DSC     | 1       | 0   | 1   | 0   |
| 2    | pf      | 1       | 0   | 0   | 1   |
| 3    | AmBot   | 0       | 0   | 0   | 0   |
| 3    | DSB (†) | 0       | 0   | 0   | 0   |
| 3    | MILK    | 0       | 0   | 0   | 0   |
| 3    | nh      | 0       | 0   | 0   | 0   |
| 3    | NPFTD   | 0       | 0   | 0   | 0   |
| 3    | PP      | 0       | -   | 0   | 0   |
| 3    | GKG     | 0       | -   | -   | 0   |

**Arena Slaughterer (Fighter)** (Enemy fighters taken (directly) Out of Action in Arena matches per **Fighter** – updated after every match):

| Rank | Fighter name     | Fighter ID | Overall | P1  | P2  | P3  |
| ---  | ---              | ---        | ---     | --- | --- | --- |
| 1    | Fred             | P14-01     | 3       | 1   | 2   | 1   |
| 2    | Alpha Garyn      | DSC-01     | 1       | 0   | 1   | 1   |
| 3    | Albert van Tulpe | pf-ß4      | 1       | 0   | 0   | 1   |

## Phase 3 matches

Matches are sorted chronologically earlier meeting/up to later meeting/down (the Match IDs of T(erritory) matches were provided at the begin of the phase which means that a higher T(erritory) Match ID can be listed before a lower because the according players met earlier for their match):

| ✓¹  | Match ID | Gangs/Fighters involved (GR/TC²)         | Winner | Territory             | Scenario                    | Att | Def   | Underdog XP for | Underdog Tactics for | Notes |
| --- | ---      | ---                                      | ---    | ---                   | ---                         | --- | ---   | ---             | ---                  | ---   |
| ✓   | P3N1     | **GKG** (500) vs **P14** (640)           | GKG    | (Non-Territory Fight) | Cauldron of Lies (HoS p108) | P14 | GKG   | 9XP for GKG     | -                    | -     |
| ✓   | P3A1     | **GKG-05** (95, H) vs **pf-04** (145, H) | pf     | (Non-Territory Fight  | 417-1: Arena Fight          | -   | -     | (Holo mode)     | -                    | -     |
| ✓   | P3N3     | **GKG** (655) vs **pf** (1960)           | pf     | (Non-Territory Fight) | Caravan Heist (p170)        | pf  | GKG   | 10XP for GKG    | (GKG but not used)   | -     |
| ✓   | P3T1     | **GKG**+**P14** (840+1700=2540) VS **DSC** (1740) | GKG+P14 | GKG: Drinking Hole (p102) | Verzweifelter Raub (WD 476p123) | DSC | GKG+P14 | 10XP for DSC | DSC | - |
| ✓   | P3N4     | **GKG**+**P14** (1165+1680=2845) VS **DSC**+**AmBot** (1975+550=2525) | - | (Non-Territory Fight) | Border Dispute (p130) | DSC+AmBot | GKG+P14 | - | - | Tie (match could not be finished) |
| ✓   | P3N5     | **P14** (~2000) VS **pf**+**DSC**+**AmBot** (~2000) | - | (Non-Territory Fight) | [Escape the Deep](https://www.warhammer-community.com/wp-content/uploads/2023/06/jtkXC36uWdmmhcQR.pdf) | pf+DSC+AmBot | P14 | - | - | Campaign end/transition match (not counted in statistics) |

* ¹ column to indicate if match results are already part of the phase end results, "✓" everything is ok, "X" means something is missing (match results not submitted, for example).
* ² Gang Rating/Total Cost value as in your Pre-Battle Sequence step "Check Gang Ratings" (not later, not earlier)

### Scenario pool for Non-Territory Matches

For a Non-Territory Fight which is not an Arena Fight choose a scenario listed under "arbitrator tools" on p160-180 and create a Match ID or choose a match from the following list: 

- P3N?: Escort Mission (p174)
- P3N?: The Hit (p182)
- P3N?: Murder Cyborg (p178)

## Phase 2 matches

Matches are sorted chronologically earlier meeting/up to later meeting/down (the Match IDs of T(erritory) matches were provided at the begin of the phase which means that a higher T(erritory) Match ID can be listed before a lower because the according players met earlier for their match):

| ✓¹  | Match ID | Gangs/Fighters involved (GR/TC²)       | Winner | Territory                  | Scenario              | Att | Def   | Underdog XP for | Underdog Tactics for | Notes |
| --- | ---      | ---                                    | ---    | ---                        | ---                   | --- | ---   | ---             | ---                  | ---   |
| ✓   | P2N1     | **PP** (500) vs **DSC** (624)          | DSC    | (Non-Territory Fight)      | Escort Mission (p174) | PP  | DSC   | +1 for PP       | -                    | PP01 (Leader) captured by DSC |
| ✓   | P2N2     | **PP** (295) vs **DSC** (830)          | DSC    | -                          | Rescue Mission (p143) | PP  | DSC   | +5 for PP       | for PP               | PP01 (Leader) could not be rescued and became an "incubator" for new DSC fighters |
| ✓   | P2T1     | **pf** (980) vs **DSC** (890)          | DSC    | Tunnels (Orlock, p105)     | Ambush (p128)         | pf  | DSC   | -               | -                    | -     |
| ✓   | P2A1     | **pf-04** (60) vs **P14-01** (195)     | P14-01 | (Non-Territory Fight)      | 417-1: Arena Fight    | -   | -     | +3 for pf-04    | -                    | -     |
| ✓   | P2A2     | **DSC-01** (310) vs **NPFTD-02** (180) | DSC01  | -                          | 417-1: Arena Fight    | -   | -     | +3 for NPFTD-02 | -                    | -     |
| ✓   | P2N3     | **pf** (1220) vs **P14** (680)         | pf     | (Non-Territory Fight)      | Caravan Heist (p170)  | pf  | P14   | +5 for P14      | for P14              | -     |
| ✓   | P2T2     | **P14** (785) vs **NPFTD** (550)       | NPFTD  | Promethium Cache (p99)     | Smash & Grab (p144)   | P14 | NPFTD | +2 for NPFTD    | -                    | -     |
| ✓   | P2A3     | **pf-04** (40) vs **P14-01** (195)     | P14-01 | (Non-Territory Fight)      | Arena Fight           | -   | -     | +4 for pf-04    | -                    | -     |
| ✓   | P2A4     | **pf-04** (125) vs **P14-01** (300)    | P14-01 | (Non-Territory Fight)      | Arena Fight           | -   | -     | +3 for pf-04    | -                    | -     |
| X   | P2T3     | **nh** (???) vs **PP** (???)           | ???    | Corpse Farm (Cawdor, p101) | Border Dispute (p130) | ??? | ???   | ?               | ?                    | Match not played |

* ¹ column to indicate if match results are already part of the phase end results, "✓" everything is ok, "X" means something is missing (match results not submitted, for example).
* ² Gang Rating/Total Cost value as in your Pre-Battle Sequence step "Check Gang Ratings" (not later, not earlier)

## Phase 1 matches

Matches are sorted chronologically earlier meeting/up to later meeting/down (the Match IDs of T(erritory) matches were provided at the begin of the phase which means that a higher T(erritory) Match ID can be listed before a lower because the according players met earlier for their match):

| ✓¹  | Match ID | Gangs/Fighters involved (GR/TC²)   | Winner | Territory                    | Scenario             | Att | Def   | Underdog XP for | Underdog Tactics for | Notes |
| --- | ---      | ---                                | ---    | ---                          | ---                  | --- | ---   | ---             | ---                  | ---   |
| ✓   | P1T2     | **pf** (500) vs **AmBot** (500)    | pf     | Fighting Pit (Goliath, p104) | Sneak Attack (p142)  | pf  | AmBot | -               | -                    | -     |
| ✓   | P1N1     | **pf** (500) vs **P14** (500)      | pf     | (Non-Territory Fight)        | Shootout (p166)      | -   | -     | -               | -                    | -     |
| ✓   | P1A1     | **pf-04** (25) vs **P14-01** (195) | P14-01 | (Non-Territory Fight)        | 417-1: Arena Fight   | -   | -     | +4 for pf-04    | -                    | -     |
| ✓   | P1N2     | **pf** (640) vs **DSB (†)** (500)  | pf     | (Non-Territory Fight)        | Sneak Attack (p142)  | pf  | DSB   | +1 for DSB      | -                    | -     | DSB: Death of Leader, end of gang |
| ✓   | P1T1     | **DSC** (500) vs **P14** (500)     | DSC    | Old Ruins (p99)              | Sabotage (p132)      | DSC | P14   | -               | -                    | -     |
| ✓   | P1N4     | **pf** (420) vs **NPFTD** (500)    | -      | (Non-Territory Fight)        | Caravan Heist (p170) | pf  | NPFTD | -               | -                    | -     |
| /   | ~~P1T3~~ | ~~**NPFTD** vs **MILK**~~          | -      | Corpse Farm (Cawdor, p101)   | Sneak Attack (p142)  | -   | -     | -               | -                    | Match not played |
| /   | ~~P1T4~~ | ~~**nh** vs **DSC**~~              | -      | Tunnels (Orlock, p105)       | Looters (p126)       | -   | -     | -               | -                    | Match not played |

* ¹ column to indicate if match results are already part of the phase end results, "✓" everything is ok, "X" means something is missing (match results not submitted, for example).
* ² Gang Rating/Total Cost value as in your Pre-Battle Sequence step "Check Gang Ratings" (not later, not earlier)

# FAQ and Errata

## Can I start over with a new Gang?

Changing your registered Gang is allowed any time but this means a complete startover for you (any progress you achieved is lost, all your territories become unoccupied).

## Does Territories grant Boons for Non-Territory Fights as well?

Yes (because a Non-Territory Fight can cause your gang casualties in the same way as a Territory Fights).

## How does a weapon with the Versatile trait effect opponents behind cover?

If the opponent behind covers goes prone and is not visible anymore, the opponent can not be attacked anymore (neither by general attacks using a Versatile weapon nor with Coup de Grace using a Versatile weapon).

## How does bonus fighters are treated related to smaller crew sizes?

Was discussed in phase 1 and the majority voted for "let's look how it works with standard rules". So nothing is changed. Tactics and Boons generate the amount if bonus fighters that are listed

## How should Bottle Tests with smaller crew sizes be handled?

This is up to you, see step "® © Decide the Bottle Test method" of the Pre-Battle Sequence in the Campaign Reference.

## Redistributing weapons amongst gang fighters

According to the offical rules Gangers would never ever give away their weapons (only wargear) because they have a strong bindings to this kind of "Underhive life policy". However, sometimes players would like to redistribute weapons anyway so in this campagin weapon redistribution is allowed in the way described by step "Distribute Equipment" in the Post-Battle Sequence of the [417 Dominion Campaign Reference](https://gitlab.com/r4dh4l/417dominion/-/blob/main/campaign_reference.pdf).

# Templates

## Match result submission

```
| ✓¹  | Match ID | Gangs/Fighters involved (GR/TC²) | Winner | Territory              | Scenario             | Att | Def   | Underdog XP for | Underdog Tacticts for | Notes |
| --- | ---      | ---                              | ---    | ---                    | ---                  | --- | ---   | ---             | ---                   | ---   |
| ✓   | PxT/N/A? | **GangID1** (???) vs **GangID2** (???)   | IF?    | (Non-Territory Fight?) | Scenario Name (p???) | ID1 | ID2   | +? for ID?      | -                     | -     |
```

Replace:
* `MatchID` with the according Match ID (see match tables above of generate a free one)
* `GangID1/2` with the IDs of the gangs participated (see participants tables above)

Examples:

| ✓¹  | Match ID | Gangs/Fighters involved (GR/TC²)   | Winner | Territory             | Scenario              | Att | Def   | Underdog XP for | Underdog Tacticts for | Notes |
| --- | ---      | ---                                | ---    | ---                   | ---                   | --- | ---   | ---             | ---                   | ---   |
| ✓   | P1A1     | **pf-04** (25) vs **P14-01** (195) | P1401  | (Non-Territory Fight) | 417-1: Arena Fight    | -   | -     | +4 for pf04     | -                     | -     |
| ✓   | P2N1     | **PP** (500) vs **DSC** (624)      | DSC    | (Non-Territory Fight) | Escort Mission (174)  | PP  | DSC   | +1 for PP       | -                     | PP01 (Leader) captured by DSC |

* ¹ column to indicate if match results are already part of the phase end results, "✓" everything is ok, "X" means something is missing (match results not submitted, for example).
* ² Gang Rating/Total Cost value as in your Pre-Battle Sequence step "Check Gang Ratings" (not later, not earlier)

## Gang status at the end of a campaign phase

**Only at the end of a campaign phase** (will be announced by the arbitrator) send the following information to the arbitrator:

```
* **GangID**:
  * Amount of occupied Territories:
  * Battles fought: (total amount of Non-Territory and Territory Matches fought, except Arena fights)
  * Arena matches: (total amount of Arena matches fought, inlcuding Arena fights)
  * Reputation: (your Gang Rating at the end of your last Post-Battle Sequence at the end of the announced campaign phase end)
  * Wealth: (Gang Rating + Stash stuff of your gang in credits at the end of your last Post-Battle Sequence at the campaign phase end)
  * Total Fighters taken Out of Action in
      * (Non-)Territory matches: (amounf of fighters your gang took Out of Action in (Non-)Territory Fights)
      * Arena matches: (amounf of fighters your gang took Out of Action in Arena Fights)
          * by ganger:
              * FighterIDofGangerParticipatedInArena: (amounf of fighters the ganger took Out of Action in all Arena Fights during the campaign phase)
```

* Replace `<GangID>` with the ID of your gang
* All values should contain the values of preceeding phases, so a value for the end of phase 2 contains the values of the end of phase 1!

Example:

* **AmBot**:
  * Amount of occupied Territories: 0
  * Battles fought: 1
  * Arena matches: 0
  * Reputation: 3
  * Wealth: 594
  * Total Fighters taken Out of Action
      * in (Non-)Territory matches: 2
      * in Arena matches: 3
          * by ganger:
              * AmBot01: 1
              * AmBot02: 2
