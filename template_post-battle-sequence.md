**4. Post-Battle Sequence: YourNameOfGangHere**

**4.1. Check Fighter status (Wrap-Up)**
4.1.1. ® Succumbing to Injuries:

**4.2. ® (Re)Assign Territory**

**4.3. Get match rewards**
4.3.1. ® Scenario rewards:

*4.3.2. ® Gaining eXPerience*
4.3.2.1. ® General XP:
4.3.2.2. ® Scenario XP:

4.3.3. ® Rewards of a Territory Fight

*4.3.4. © ® Rewards from the Arbitrator*
4.3.4.1. © ® Underdog eXPerience:
4.3.4.2. © ® REP blessings by the Arbitrator:
4.3.4.2.1. General Arbitrator REP:
4.3.4.2.2. Arbitrator REP for Winner/Looser:
4.3.4.3. © ® ArcheoTech Hunt:

**4.4. ® Collect territory income**

**4.5. Make Post-Battle Actions**
*4.5.1. ® Seek rare (legal and illegal) items*
4.5.1.1. Choose gangers to improve seeking rare-legal/illegal items:
4.5.1.1.1. Seek rare-legal items (Post-BA):
4.5.1.1.2. Seek rare-illegal items (Post-BA):
*4.5.1.2. Determine rare-legal/illegal availability*
- 4.5.1.2.1. Determine rare-legal availability:
- 4.5.1.2.2. Determine rare-illegal availability:

*4.5.2. Actions related to captives*
4.5.2.1. ® Trade captives:
4.5.2.2. ® Claim bounties for captives:
4.5.2.3. ® „Dispose“ captives:

4.5.3. ® Make a medical escort:

4.5.4. ® Negotiate vehicle repairs:

*4.5.5. © ® Visit an ArcheoTechnician*
4.5.5.1. © ® Analyze unidentified ArcheoTech:
4.5.5.2. © ® Repair damaged ArcheoTech:

**4.6. Update Roster**
4.6.1. ® Clean House:
4.6.2. ® Visit the Trading Post:
4.6.3. ® Gain Boons from Territories:
4.6.4. ® Distribute Equipment:
4.6.5. © ® Purchase Advancements:
4.6.6. ® Update Gang Rating:
4.6.7. ® Update Wealth:

**4.7. © ® Submit match results**
